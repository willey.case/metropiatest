//
//  ViewModelUnitTest.swift
//  MetropiaTestTests
//
//  Created by Yu-Hsin Hsiao on 2022/1/5.
//

import XCTest
import heresdk
@testable import MetropiaTest

class ViewModelUnitTest: XCTestCase {
    
    var viewModel = ViewModel()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        viewModel.searchEngine = nil
    }
    
    func test_buildSearchEngine() {
        viewModel.buildSearchEngine()
        XCTAssertNotNil(viewModel.searchEngine)
    }
    
    func test_makePOIMapMarker() {
        let coodinates = GeoCoordinates(latitude: viewModel.locationManager.center.latitude,
                                        longitude: viewModel.locationManager.center.longitude)
        let marker = viewModel.makePOIMapMarker(coodinates)
        XCTAssertNotNil(marker)
    }
    
    func test_clearMapMarkers() {
        let coodinates = GeoCoordinates(latitude: viewModel.locationManager.center.latitude,
                                        longitude: viewModel.locationManager.center.longitude)
        _ = viewModel.makePOIMapMarker(coodinates)
        _ = viewModel.makePOIMapMarker(coodinates)
        _ = viewModel.makePOIMapMarker(coodinates)
        
        // 清除 MapMarkers
        viewModel.clearMapMarkers()
        
        let expec = XCTestExpectation(description: "test_clearMapMarkers")
        
        viewModel.mapMarkersDidClear = {(markers) in
            XCTAssert(markers.count == 0)
            expec.fulfill()
        }
        
        viewModel.clearMapMarkers()
        
        self.wait(for: [expec], timeout: 3)
    }
    
    func test_getAddressForCoordinates() {
        viewModel.buildSearchEngine()
        
        let coodinates = GeoCoordinates(latitude: viewModel.locationManager.center.latitude,
                                        longitude: viewModel.locationManager.center.longitude)
        
        let expec = XCTestExpectation(description: "test_getAddressForCoordinates")
        
        viewModel.addressDidGet = { place in
            expec.fulfill()
        }
        
        viewModel.getAddressForCoordinates(geoCoordinates: coodinates)
        
        self.wait(for: [expec], timeout: 5)
    }
}
