//
//  NetworkManager.swift
//  MetropiaTest
//
//  Created by Yu-Hsin Hsiao on 2022/1/5.
//

import Network
import Foundation

protocol NetworkManagerDelegate: class {
    
    func networkConnectChanged(_ isConnect: Bool) -> Void
}

class NetworkManager {
    
    /// Singleton
    static let shared = NetworkManager()
    
    private var pathMonitor: NWPathMonitor!
    private var path: NWPath? {
        didSet {
            delegate?.networkConnectChanged(isNetworkConnect)
        }
    }
    private lazy var pathUpdateHandler: ((NWPath) -> Void) = {[weak self] path in
        self?.path = path
        if path.status == NWPath.Status.satisfied {
            NSLog("Network Connected")
        } else if path.status == NWPath.Status.unsatisfied {
            NSLog("Network unsatisfied")
        } else if path.status == NWPath.Status.requiresConnection {
            NSLog("Network requiresConnection")
        }
    }
    private let pathQueue = DispatchQueue(label: "NetworkManager_PathQueue")
    
    var isNetworkConnect: Bool {
        guard let path = path else {return false}
        return path.status == .satisfied
    }
    weak var delegate: NetworkManagerDelegate?
    
    init() {
        pathMonitor = NWPathMonitor()
        pathMonitor.pathUpdateHandler = self.pathUpdateHandler
        pathMonitor.start(queue: pathQueue)
    }
}
