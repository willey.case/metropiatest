//
//  LocationManager.swift
//  MetropiaTest
//
//  Created by Yu-Hsin Hsiao on 2022/1/3.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: NSObjectProtocol {
    
    func firstTimeUpdateLocation() -> Void
}

/// Location singleton service
class LocationManager: NSObject {
    
    static let shared = LocationManager()
    
    private let locManager: CLLocationManager
    /// 進到畫面後，是否為第一次取得Location
    private var firstUpdateLocation: Bool = true
    /// 預設座標，台北101
    var center: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 25.03416375861882,
                                                                longitude: 121.5645453693753)
    var authorizationStatus: CLAuthorizationStatus {
        return locManager.authorizationStatus
    }
    weak var delegate: LocationManagerDelegate?
    
    override init() {
        self.locManager = CLLocationManager()
        super.init()
        self.locManager.delegate = self
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locManager.startUpdatingLocation()
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        NSLog("Manager status = \(manager.authorizationStatus), rawValue = \(manager.authorizationStatus.rawValue)")
        switch manager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            locManager.startUpdatingLocation()
        case .denied, .notDetermined, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        NSLog("Location = \(locations)")
        /// 紀錄最後的位置在哪
        if let last = locations.last {
            center.latitude = last.coordinate.latitude
            center.longitude = last.coordinate.longitude
            
            if firstUpdateLocation {
                firstUpdateLocation = false
                delegate?.firstTimeUpdateLocation()
            }
        }
    }
}
