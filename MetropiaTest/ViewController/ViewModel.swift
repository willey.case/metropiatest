//
//  ViewModel.swift
//  MetropiaTest
//
//  Created by Yu-Hsin Hsiao on 2022/1/4.
//

import UIKit
import heresdk
import Foundation

class ViewModel: NSObject {
    let locationManager: LocationManager
    let networkManager: NetworkManager
    var searchEngine: SearchEngine?
    let mapZoomLevel: Double = 13
    var shouldUpdateLocation: (() -> Void)?
    var networkConnectDidChanged: ((_ isConnect: Bool) -> Void)?
    private var mapMarkers: [MapMarkerLite] = []
    var mapMarkersDidClear: ((_ makers: [MapMarkerLite]) -> Void)?
    private var searchTask: TaskHandle?
    var addressDidGet: ((_ place: Place?) -> Void)?
    
    init(locManager: LocationManager = LocationManager.shared,
         networkManager: NetworkManager = NetworkManager.shared) {
        self.locationManager = locManager
        self.networkManager = networkManager
        super.init()
        locationManager.delegate = self
        networkManager.delegate = self
    }
    
    // Note: 搜尋引擎必須要在Map 載入以後，成功啟動SDK才能初始化
    func buildSearchEngine() {
        do {
            try searchEngine = SearchEngine()
        } catch let error {
            print("Init searchEngine error \(error)")
        }
    }
    
    func makePOIMapMarker(_ coordinates: GeoCoordinates) -> MapMarkerLite? {
        let mapMarker = MapMarkerLite(at: coordinates)
        // Drag & Drop the image to Assets.xcassets (or simply add the image as file to the project).
        // You can add multiple resolutions to Assets.xcassets that will be used depending on the
        // display size.
        guard let image = UIImage(named: "pin_location"),
              let mapImage = MapImageLite(image) else {return nil}
        
        // The bottom, middle position should point to the location.
        // By default, the anchor point is set to 0.5, 0.5.
        let mapMarkerImageStyle = MapMarkerImageStyleLite()
        mapMarkerImageStyle.setAnchorPoint(Anchor2D(horizontal: 0.5, vertical: 1))

        mapMarker.addImage(mapImage, style: mapMarkerImageStyle)
        mapMarkers.append(mapMarker)
        return mapMarker
    }
    
    func clearMapMarkers() {
        mapMarkersDidClear?(mapMarkers)
        mapMarkers.removeAll()
    }
    
    // 加分題二 綠色框：移動地圖後，用螢幕的中心點座標查詢出該地點的地址並顯示在螢幕上
    
    func getAddressForCoordinates(geoCoordinates: GeoCoordinates) {
        
        if let task = searchTask, !task.isFinished {
            // 先取消進行中的SearchTask
            if let result = searchTask?.cancel() {
                // Cancel失敗則直接return
                if !result {
                    return
                }
            }
        }
        
        // By default results are localized in EN_US.
        let reverseGeocodingOptions = SearchOptions(languageCode: LanguageCode.zhTw,
                                                    maxItems: 1)
        
        // 建立新的SearchTask
        searchTask = searchEngine?.search(coordinates: geoCoordinates,
                                          options: reverseGeocodingOptions,
                                          completion: onReverseGeocodingCompleted)
    }

    // Completion handler to receive reverse geocoding results.
    func onReverseGeocodingCompleted(error: SearchError?, items: [Place]?) {
        if let error = error {
            NSLog("Error: Get address, \(error)")
            addressDidGet?(nil)
            return
        }
        addressDidGet?(items?.first)
    }
}



// MARK: - LocationManagerDelegate

extension ViewModel: LocationManagerDelegate {
    
    func firstTimeUpdateLocation() {
        shouldUpdateLocation?()
    }
}

// MARK: - NetworkManagerDelegate

extension ViewModel: NetworkManagerDelegate {
    
    func networkConnectChanged(_ isConnect: Bool) {
        networkConnectDidChanged?(isConnect)
    }
}
