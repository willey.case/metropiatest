//
//  ViewController.swift
//  MetropiaTest
//
//  Created by Yu-Hsin Hsiao on 2022/1/3.
//

import UIKit
import heresdk

class ViewController: UIViewController {
    
    let viewModel = ViewModel()
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var mapView: MapViewLite!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var centerButton: UIButton! {
        didSet {
            centerButton.layer.cornerRadius = 22
        }
    }
    @IBOutlet weak var noNetworkView: UIView! {
        didSet {
            noNetworkView.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var loadingView: UIView! {
        didSet {
            loadingView.alpha = 0.3
        }
    }
    
    deinit {
        mapView.camera.removeObserver(self)
    }
    
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewModel()
        viewModel.buildSearchEngine()
        noNetworkView.isHidden = viewModel.networkManager.isNetworkConnect
        mapView.camera.addObserver(self)
        // 加分題一 紅色框：增加手勢可以移動地圖，並加一個按鈕，按了可以將目前位置定位在螢幕的中心點
        // 新增雙擊手勢，將點擊位置至於地圖中心
        mapView.gestures.doubleTapDelegate = self
        // Disable the default map gesture behavior for DoubleTap (zooms in)
        mapView.gestures.disableDefaultAction(forGesture: .doubleTap)
        let config = MapSceneConfigLite(mainLanguageCode: LanguageCode.zhTw,
                                        fallbackLanguageCode: LanguageCode.enUs)
        mapView.mapScene.loadScene(mapStyle: .normalDay,
                                   sceneConfig: config,
                                   callback: onLoadScene)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        mapView.handleLowMemory()
    }
    
    /// 加分題一 紅色框：增加手勢可以移動地圖，並加一個按鈕，按了可以將目前位置定位在螢幕的中心點
    @IBAction func centerButtonDidPressed(_ sender: UIButton) {
        if viewModel.locationManager.authorizationStatus == .denied {
            showDialog(title: "Location Denied",
                       message: "Please enable location service in app settings")
            return
        }
        setMapViewCenter()
    }
    
    func onLoadScene(errorCode: MapSceneLite.ErrorCode?) {
        showLoadingView(false)
        if let error = errorCode {
            showDialog(title: "Map Scene", message: "Error: Map scene not loaded, \(error)")
        } else {
            setMapViewCenter()
        }
    }
    
    
    
    // MARK: - Private 
    
    private func setViewModel() {
        
        viewModel.shouldUpdateLocation = {[weak self] in
            DispatchQueue.main.async {
                self?.setMapViewCenter()
            }
        }
        
        viewModel.networkConnectDidChanged = {[weak self] isConnect in
            DispatchQueue.main.async {
                self?.noNetworkView.isHidden = isConnect
                
                // 取得網路後，更新畫面中心地址
                if isConnect,
                   let target = self?.mapView.camera.getTarget() {
                    self?.viewModel.getAddressForCoordinates(geoCoordinates: target)
                }
            }
        }
        
        viewModel.mapMarkersDidClear = {[weak self] markers in
            DispatchQueue.main.async {
                for marker in markers {
                    self?.mapView.mapScene.removeMapMarker(marker)
                }
            }
        }
        
        viewModel.addressDidGet = {[weak self] (place) in
            DispatchQueue.main.async {
                guard let self = self,
                      let place = place else {return}
                
                self.addressLabel.text = place.address.addressText
            }
        }
    }
    
    private func setMapViewCenter() {
        
        viewModel.clearMapMarkers()
        
        // Configure the map.
        let coordinates = GeoCoordinates(latitude: viewModel.locationManager.center.latitude,
                                         longitude: viewModel.locationManager.center.longitude)
        mapView.camera.setTarget(coordinates)
        mapView.camera.setZoomLevel(viewModel.mapZoomLevel)
        
        // 新增 MapMaker
        guard let mapMarker = viewModel.makePOIMapMarker(coordinates) else {return}
        mapView.mapScene.addMapMarker(mapMarker)
    }
    
    private func showLoadingView(_ isShow: Bool) {
        loadingView.isHidden = !isShow
    }
    
    private func showDialog(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}


// MARK: - CameraObserverLite

extension ViewController: CameraObserverLite {
    
    // 加分題二 綠色框：移動地圖後，用螢幕的中心點座標查詢出該地點的地址並顯示在螢幕上
    func onCameraUpdated(_ cameraUpdate: CameraUpdateLite) {
        let mapCenter = cameraUpdate.target
        viewModel.getAddressForCoordinates(geoCoordinates: mapCenter)
        NSLog("Current map center location: \(mapCenter). Current zoom level: \(cameraUpdate.zoomLevel)");
    }
}



// MARK: - DoubleTapDelegate

extension ViewController: DoubleTapDelegate {
    
    /**
     加分題一 紅色框：增加手勢可以移動地圖，並加一個按鈕，按了可以將目前位置定位在螢幕的中心點
     新增雙擊手勢，將點擊位置至於地圖中心
     */
    func onDoubleTap(origin: Point2D) {
        let geoCoordinates = mapView.camera.viewToGeoCoordinates(viewCoordinates: origin)
        mapView.camera.setTarget(geoCoordinates)
        viewModel.clearMapMarkers()
        if let mapMarker = viewModel.makePOIMapMarker(geoCoordinates) {
            self.mapView.mapScene.addMapMarker(mapMarker)
        }
    }
}

